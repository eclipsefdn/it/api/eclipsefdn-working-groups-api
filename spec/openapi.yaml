openapi: "3.1.0"
info:
   version: 1.0.0
   title: Working Groups API
   description: Access information on Eclipse Foundation Working Groups.
   license:
      name: Eclipse Public License - 2.0
      url: https://www.eclipse.org/legal/epl-2.0/

servers:
   - url: https://api.eclipse.org/working-groups
     description: Production endpoint for Eclipse Foundation working group data

paths:
   /:
      parameters:
         - name: status
           in: query
           schema:
            type: string
            description: The project status
      get:
         tags:
            - Working Groups
         summary: Working Group List
         description: Returns a list of working groups that have the desired status
         responses:
            200:
               description: Success
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/WorkingGroups"
            500:
               description: Error while retrieving data

   /{alias}:
      parameters:
         - name: alias
           in: path
           description: The name of the working group to retrieve
           required: true
           schema:
              type: string
      get:
         summary: Working Group
         description: Returns a working group entry that has a matching alias
         responses:
            200:
               description: Success
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/WorkingGroup"
            404:
               description: Not Found
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/Error"
            500:
               description: Error while retrieving data

   /{alias}/resources:
      parameters:
         - name: alias
           in: path
           description: The name of the working group to retrieve
           required: true
           schema:
              type: string
      get:
         summary: Working Group resources
         description: Returns the resources info for the WG with the specified alias
         responses:
            200:
               description: Success
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/WorkingGroupResources"
            404:
               description: Not Found
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/Error"
            500:
               description: Error while retrieving data

   /{alias}/levels:
      parameters:
         - name: alias
           in: path
           description: The name of the working group to retrieve
           required: true
           schema:
              type: string
      get:
         summary: Working Group levels
         description: Returns the levels info for the WG with the specified alias
         responses:
            200:
               description: Success
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/WorkingGroupLevels"
            404:
               description: Not Found
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/Error"
            500:
               description: Error while retrieving data

   /{alias}/agreements:
      parameters:
         - name: alias
           in: path
           description: The name of the working group to retrieve
           required: true
           schema:
              type: string
      get:
         summary: Working Group agreements
         description: Returns the participation agreements info for the WG with the specified alias
         responses:
            200:
               description: Success
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/WorkingGroupParticipationAgreements"
            404:
               description: Not Found
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/Error"
            500:
               description: Error while retrieving data

   /{alias}/committees/{relationType}:
      parameters:
         - name: alias
           in: path
           description: The name of the working group to retrieve
           required: true
           schema:
              type: string
         - name: relationType
           in: path
           description: The prefix of the WG committee relation codes
           required: true
           schema:
              type: string
      get:
         summary: Working Group Committee
         description: Returns the WG Committee info for the WG with the specified alias and group prefix
         responses:
            200:
               description: Success
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/WorkingGroupCommittee"
            400:
               description: Invalid alias or relation type
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/Error"
            500:
               description: Error while retrieving data
   /organization/{id}:
      parameters:
         - name: id
           in: path
           description: The ID of the organization to retrieve working groups for
           required: true
           schema:
              type: integer
      get:
         summary: Working Group by Organization ID
         description: Returns working groups that the target organization is a member of 
         responses:
            200:
               description: Success
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/WorkingGroups"
            404:
               description: Not Found
               content:
                  application/json:
                     schema:
                        $ref: "#/components/schemas/Error"
            500:
               description: Error while retrieving data
components:
   securitySchemes:
      openId:
         type: openIdConnect
         openIdConnectUrl: https://auth.eclipse.org/auth/realms/foundation/.well-known/openid-configuration

   schemas:
      ObjectID:
         description: Unique identifier for an addressable object in the API. Can be null when posting or updating objects in the API if part of the root element or is a new entity. Should always be set in responses.
         type:
            - string
            - "null"
         minimum: 1

      WorkingGroups:
         type: array
         items:
            $ref: "#/components/schemas/WorkingGroup"

      WorkingGroup:
         type: object
         additionalProperties: false
         required:
            - alias
            - title
            - status
            - logo
            - description
            - resources
            - levels
         properties:
            alias:
               type: string
               description: internal alias for the working group
            title:
               type: string
               description: Public-facing or official name of the working group
            status:
               type: string
               description: Current status of the working group (e.g. incubating, active, archived)
            logo:
               type: string
               description: image link for the working groups logo
            description:
               type: string
               description: Short description of the working group
            resources:
               $ref: "#/components/schemas/WorkingGroupResources"
            levels:
               $ref: "#/components/schemas/WorkingGroupLevels"

      WorkingGroupResources:
         type: object
         additionalProperties: false
         required:
            - charter
            - contact_form
            - members
            - participation_agreements
            - sponsorship
            - website
         properties:
            charter:
               type: string
               description: Link to the charter for the current working group
            contact_form:
               type: string
               description: URL link to the contact form
            members:
               type: string
               description: URL to the members list for the given working group
            participation_agreements:
               $ref: "#/components/schemas/WorkingGroupParticipationAgreements"
            sponsorship:
               type: string
               description: link to the sponsorship agreement document
            website:
               type: string
               description: the URL for the homepage of this working group

      WorkingGroupParticipationAgreements:
         type: object
         additionalProperties: false
         properties:
            individual:
               $ref: "#/components/schemas/WorkingGroupParticipationAgreement"
            organization:
               $ref: "#/components/schemas/WorkingGroupParticipationAgreement"

      WorkingGroupParticipationAgreement:
         type:
            - object
            - "null"
         additionalProperties: false
         properties:
            document_id:
               $ref: "#/components/schemas/ObjectID"
            pdf:
               type: string
               description: Link to the PDF version of the participation agreement

      WorkingGroupLevels:
         type: array
         items:
            $ref: "#/components/schemas/WorkingGroupLevel"

      WorkingGroupLevel:
         type: object
         additionalProperties: false
         required:
            - relation
            - description
         description: Definition of the participation level for the working group, including its relation code
         properties:
            relation:
               type: string
               description: code representing the relation, used internally when using common relation types
            description:
               type: string
               description: the label for the participation level

      WorkingGroupCommittee:
         type: object
         additionalProperties: false
         required:
            - group_prefix
            - working_group
            - appointed_primary
            - appointed_alternate
            - elected_primary
            - elected_alternate
         description: Definition of the WG committee
         properties:
            group_prefix:
               type: string
               description: Prefix of the code representing the relation, used internally when using common relation types.
            working_group:
               type: string
               description: The WG alias.
            appointed_primary:
               $ref: "#/components/schemas/CommitteeRole"
            appointed_alternate:
               $ref: "#/components/schemas/CommitteeRole"
            elected_primary:
               $ref: "#/components/schemas/CommitteeRole"
            elected_alternate:
               $ref: "#/components/schemas/CommitteeRole"
            committer_representative:
               $ref: "#/components/schemas/CommitteeRole"
            invited_guest:
               $ref: "#/components/schemas/CommitteeRole"

      CommitteeRole:
         type: object
         additionalProperties: false
         required:
            - code
            - description
            - members
         description: The committee position.
         properties:
            code:
               type: string
               description: The code representing the relation. Used internally when using common relation types.
            description:
               type: string
               description: The relation description.
            members:
               type: array
               items:
                  $ref: "#/components/schemas/CommitteeMember"

      CommitteeMember:
         type: object
         additionalProperties: false
         required:
            - name
            - username
            - organization
         description: A committee member representative.
         properties:
            name:
               type: string
               description: The name of the representative.
            username:
               type: string
               description: The representative's EF username.
            organization:
               $ref: "#/components/schemas/MemberOrganization"

      MemberOrganization:
         type: object
         additionalProperties: false
         required:
            - name
            - organization_id
            - logos
         description: A member representative's organization info.
         properties:
            name:
               type: string
               description: The organization name.
            organization_id:
               type: integer
               description: The organization id.
            logos:
               $ref: "#/components/schemas/MemberOrganizationLogos"

      MemberOrganizationLogos:
         type: object
         description: A member organization's logo data
         properties:
            print:
               type:
                  - string
                  - "null"
               description: The print logo URL.
            web:
               type:
                  - string
                  - "null"
               description: The web logo URL.

      Error:
         type: object
         additionalProperties: false
         required:
            - status_code
            - message
         properties:
            status_code:
               type: integer
               description: HTTP response code
            message:
               type: string
               description: Message containing error information
            url:
               oneOf:
                  - type: string
                  - type: "null"
               description: The URL
