#!/bin/bash
# ===========================================================================
# Copyright (c) 2021 Eclipse Foundation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Christopher Guindon (Eclipse Foundation)
# ==========================================================================

# Generate ssl cert for jwilder/nginx-proxy
mkdir -p config/certs
if [[ ! -f  config/certs/dev.docker.key ]] || [[ ! -f  config/certs/dev.docker.crt ]]
then
  openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout config/certs/dev.docker.key -out config/certs/dev.docker.crt
fi