SHELL = /bin/bash

pre-setup:;
	@echo "Creating environment file from template"
	@rm -f .env && envsubst < config/.env.sample > .env
	[ -f ./config/mariadb/main/init.d/eclipsefoundation.sql ] && echo "EF DB dump already exists, skipping fetch" || (scp api-vm1:~/webdev/sql/eclipsefoundation.sql.gz ./config/mariadb/main/init.d/ && gzip -d ./config/mariadb/main/init.d/eclipsefoundation.sql.gz)

setup:;
	@echo "Generating secret files from templates using environment file + variables"
	@source .env && rm -f ./config/application/secret.properties && envsubst < config/application/secret.properties.sample > config/application/secret.properties
	@source .env && rm -f ./config/foundationdb/secret.properties && envsubst < config/foundationdb/secret.properties.sample > config/foundationdb/secret.properties
	@echo "Generating certs for NGINX reverse proxy" && sh ./config/dev-ssl-cert.sh
dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties

clean:;
	mvn clean
	yarn run clean

install-yarn:;
	yarn install --frozen-lockfile --audit

compile-test-resources: install-yarn;
	yarn run generate-json-schema
	yarn test

compile-java: compile-test-resources;
	mvn compile package

compile-java-quick: compile-test-resources;
	mvn compile package -Dmaven.test.skip=true

compile: clean compile-java;

compile-quick: clean compile-java-quick;

compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d

start-spec: compile-test-resources;
	yarn run start
	