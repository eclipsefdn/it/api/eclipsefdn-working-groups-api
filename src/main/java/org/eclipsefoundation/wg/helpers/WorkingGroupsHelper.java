/*********************************************************************
* Copyright (c) 2022, 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.helpers;

import java.util.ArrayList;
import java.util.List;

import org.eclipsefoundation.wg.models.WorkingGroup;
import org.eclipsefoundation.wg.models.WorkingGroup.WorkingGroupParticipationAgreement;
import org.eclipsefoundation.wg.namespace.CommitteePositionType;

/**
 * A class that contains helper methods for WG processing.
 */
public final class WorkingGroupsHelper {

    /**
     * Extracts WG Participation Agreements from the given WG object.
     * 
     * @param wg The given Workgin Group
     * @return A list of participation agreement documents
     */
    public static List<String> extractWGPADocumentIDs(WorkingGroup wg) {
        List<String> ids = new ArrayList<>();
        WorkingGroupParticipationAgreement iwgpa = wg.resources().participationAgreements().individual();
        if (iwgpa != null) {
            ids.add(iwgpa.documentId());
        }

        WorkingGroupParticipationAgreement wgpa = wg.resources().participationAgreements().organization();
        if (wgpa != null) {
            ids.add(wgpa.documentId());
        }

        return ids;
    }

    /**
     * Method used to filter fetched relations. Ensuring they start with the desired prefix, or are IG/CR position type.
     * 
     * @param relation The current relation
     * @param prefix The group prefix
     * @return true if criteria is met, false otherwise
     */
    public static boolean filterRelationsByPrefix(String relation, String prefix) {
        return relation.toLowerCase().startsWith(prefix.toLowerCase())
                || CommitteePositionType.getType(relation) == CommitteePositionType.IG
                || CommitteePositionType.getType(relation) == CommitteePositionType.CR;
    }

    /**
     * Method used to filter working groups by project status.
     * 
     * @param statuses The statuses to filter.
     * @param wg The current working group
     * @return True if WG status in list of statuses. False if not.
     */
    public static boolean filterByProjectStatuses(List<String> statuses, WorkingGroup wg) {
        return statuses == null || statuses.isEmpty() || statuses.contains(wg.status());
    }

    /**
     * Method used to filter working groups by parent organizations.
     * 
     * @param parentOrganizations The parent orgs used to filter.
     * @param wg The current working group
     * @return True if parent organization in list of provided orgs. False if not.
     */
    public static boolean filterByParentOrganizations(List<String> parentOrganizations, WorkingGroup wg) {
        return parentOrganizations == null || parentOrganizations.isEmpty() || parentOrganizations.contains(wg.parentOrganization());
    }

    private WorkingGroupsHelper() {
    }
}
