/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.wg.models.WorkingGroup;
import org.eclipsefoundation.wg.models.WorkingGroup.WorkingGroupCommittee;
import org.eclipsefoundation.wg.models.WorkingGroup.WorkingGroupParticipationAgreements;
import org.eclipsefoundation.wg.models.WorkingGroup.WorkingGroupParticipationLevel;
import org.eclipsefoundation.wg.models.WorkingGroup.WorkingGroupResources;
import org.eclipsefoundation.wg.services.WorkingGroupsService;
import org.jboss.resteasy.reactive.Cache;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestQuery;
import org.jboss.resteasy.reactive.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * Retrieves working group definitions from the working groups service.
 *
 * @author Martin Lowe, Zachary Sabourin
 */
@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
public class WorkingGroupsResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(WorkingGroupsResource.class);

    private static final int GROUP_PREFIX_LENGTH = 3;

    private final WorkingGroupsService wgService;

    public WorkingGroupsResource(WorkingGroupsService wgService) {
        this.wgService = wgService;
    }

    @GET
    @Path("")
    public RestResponse<List<WorkingGroup>> getWorkingGroups(@QueryParam("parent_organization") List<String> parentOrganization,
            @RestQuery List<String> status) {
        // return the results as a response
        return RestResponse.ok(new ArrayList<>(wgService.get(status, parentOrganization)));
    }

    @GET
    @Path("organization/{organizationId}")
    public RestResponse<List<WorkingGroup>> getWorkingGroupsForOrganization(@RestPath String organizationId) {
        // return the results as a response
        return RestResponse.ok(new ArrayList<>(wgService.getByOrganizationId(organizationId)));
    }

    @GET
    @Path("{alias}")
    public RestResponse<WorkingGroup> getWorkingGroup(@RestPath String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            throw new NotFoundException(String.format("Couldn't find working group with alias: '%s'", alias));
        }

        return RestResponse.ok(wg);
    }

    @GET
    @Path("{alias}/resources")
    public RestResponse<WorkingGroupResources> getWorkingGroupResources(@RestPath String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            throw new NotFoundException(String.format("Couldn't find resources for working group with alias: '%s'", alias));
        }

        return RestResponse.ok(wg.resources());
    }

    @GET
    @Path("{alias}/levels")
    public RestResponse<List<WorkingGroupParticipationLevel>> getWorkingGroupLevels(@RestPath String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            throw new NotFoundException(String.format("Couldn't find levels for working group with alias: '%s'", alias));
        }

        return RestResponse.ok(wg.levels());
    }

    @GET
    @Path("{alias}/agreements")
    public RestResponse<WorkingGroupParticipationAgreements> getWorkingGroupAgreements(@RestPath String alias) {
        WorkingGroup wg = wgService.getByAlias(alias);

        if (Objects.isNull(wg)) {
            throw new NotFoundException(String.format("Couldn't find agreements for working group with alias: '%s'", alias));
        }

        return RestResponse.ok(wg.resources().participationAgreements());
    }

    @GET
    @Path("{alias}/committees/{groupPrefix}")
    public RestResponse<WorkingGroupCommittee> getWorkingGroupCommittees(@RestPath String alias, @RestPath String groupPrefix) {

        if (groupPrefix.length() != GROUP_PREFIX_LENGTH) {
            throw new BadRequestException(String.format("Invalid group prefix: %s", groupPrefix));
        }

        WorkingGroupCommittee committee = wgService.getWGCommittee(alias, groupPrefix);
        if (committee == null) {
            throw new BadRequestException(String.format("Invalid committee parameters: %s, %s", alias, groupPrefix));
        }

        return RestResponse.ok(committee);
    }
}
