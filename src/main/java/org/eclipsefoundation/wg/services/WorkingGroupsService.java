/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipsefoundation.wg.models.WorkingGroup;
import org.eclipsefoundation.wg.models.WorkingGroup.WorkingGroupCommittee;

/**
 * Defines a service that will return available working group data, allowing for retrieval by alias name, or the full set.
 * 
 * @author Martin Lowe, Zachary Sabourin
 *
 */
public interface WorkingGroupsService {

    /**
     * Returns a set of working groups, with an optional filter of the project status.
     * 
     * @param projectStatus optional, statuses to include in result set
     * @return set of working groups matching optional filters if present, otherwise all available working groups
     */
    public Set<WorkingGroup> get(List<String> projectStatus, List<String> parentOrganization);

    /**
     * Returns a set of working groups, filtering to only those that the target organization is a member of.
     * 
     * @param organizationId the organization to retrieve working groups for
     * @return set of working groups for the matching organization ID
     */
    public Set<WorkingGroup> getByOrganizationId(String organizationId);

    /**
     * Returns a working group based on the given alias.
     * 
     * @param alias the WG alias
     * @return A WorkingGroup entity
     */
    public WorkingGroup getByAlias(String alias);

    /**
     * Returns a collection containing WG participation document ids mapped to each WG alias.
     * 
     * @return A map containing lists of WGPA document ids for each WG
     */
    public Map<String, List<String>> getWGPADocumentIDs();

    /**
     * Returns a WG committee using the given WG alias and group prefix
     * 
     * @param alias The WG alias.
     * @param groupPrefix The group prefix of the WG committee.
     * @return A WorkingGroupCommittee entity
     */
    public WorkingGroupCommittee getWGCommittee(String alias, String groupPrefix);
}
