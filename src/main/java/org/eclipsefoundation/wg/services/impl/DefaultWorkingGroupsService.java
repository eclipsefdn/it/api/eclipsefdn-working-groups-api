/*********************************************************************
* Copyright (c) 2022, 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		  Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.caching.service.LoadingCacheManager;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.eclipsefoundation.wg.api.FoundationDbAPI;
import org.eclipsefoundation.wg.config.WorkingGroupsConfig;
import org.eclipsefoundation.wg.helpers.WorkingGroupsHelper;
import org.eclipsefoundation.wg.models.CommitteeRole;
import org.eclipsefoundation.wg.models.CommitteeRole.CommitteeMember;
import org.eclipsefoundation.wg.models.MemberOrganization;
import org.eclipsefoundation.wg.models.MemberOrganization.MemberOrganizationLogos;
import org.eclipsefoundation.wg.models.WorkingGroup;
import org.eclipsefoundation.wg.models.WorkingGroup.WorkingGroupCommittee;
import org.eclipsefoundation.wg.models.WorkingGroupMap;
import org.eclipsefoundation.wg.namespace.CommitteePositionType;
import org.eclipsefoundation.wg.services.WorkingGroupsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cronutils.utils.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.Response.Status;

/**
 * Builds a list of working group definitions from an embedded list of working group definitions. This is an interim solution to accelerate
 * this project and should be replaced with a call to the foundation API to retrieve this data.
 * 
 * @author Martin Lowe, Zachary Sabourin
 */
@Startup
@ApplicationScoped
public class DefaultWorkingGroupsService implements WorkingGroupsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultWorkingGroupsService.class);

    private static final int INVALID_PREFIX_COUNT = 2;

    private final WorkingGroupsConfig config;
    private final FoundationDbAPI fdbAPI;
    private final LoadingCacheManager cacheManager;
    private final CachingService cache;
    private final APIMiddleware middleware;

    private final ObjectMapper json;

    private Map<String, WorkingGroup> workingGroups;

    public DefaultWorkingGroupsService(WorkingGroupsConfig config, @RestClient FoundationDbAPI fdbAPI, LoadingCacheManager cacheManager,
            CachingService cache, APIMiddleware middleware, ObjectMapper json) {
        this.config = config;
        this.fdbAPI = fdbAPI;
        this.cacheManager = cacheManager;
        this.cache = cache;
        this.middleware = middleware;
        this.json = json;
    }

    /**
     * At startup, will load the working groups and store them locally to be used throughout the servers life time. As this resource is
     * embedded within the Jar, we do not need to look for changes to the resource, as that would not happen with a production server.
     * 
     * @throws IOException if there is an issue loading the working groups resource from within the Jar resources.
     */
    @PostConstruct
    void init() throws IOException {
        LOGGER.info("Starting init of working group levels static members");

        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(config.filepath())) {
            WorkingGroupMap map = json.readValue(is, WorkingGroupMap.class);
            this.workingGroups = new HashMap<>(map.workingGroups());
            LOGGER.info("Initialized {} working groups", workingGroups.size());
            getOrgLogosById(0);
        }
    }

    @Override
    public Set<WorkingGroup> get(List<String> projectStatuses, List<String> parentOrganizations) {
        return new HashSet<>(workingGroups
                .entrySet()
                .stream()
                .filter(e -> WorkingGroupsHelper.filterByParentOrganizations(parentOrganizations, e.getValue()))
                .filter(e -> WorkingGroupsHelper.filterByProjectStatuses(projectStatuses, e.getValue()))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue))
                .values());
    }

    @Override
    public WorkingGroup getByAlias(String alias) {
        return workingGroups.get(alias);
    }

    @Override
    public Set<WorkingGroup> getByOrganizationId(String organizationId) {
        if (!StringUtils.isNumeric(organizationId)) {
            throw new BadRequestException("Organization ID should be a valid number");
        }
        Optional<MemberOrganization> org = getOrganizationById(Integer.parseInt(organizationId));
        if (org.isEmpty()) {
            throw new NotFoundException("Could not find an org with the given id: " + TransformationHelper.formatLog(organizationId));
        }

        // map the WGPA aliases to the actual working groups, dropping non matching null values
        return org.get().wgpas().stream().map(wgpa -> getByAlias(wgpa.workingGroup())).filter(Objects::nonNull).collect(Collectors.toSet());
    }

    @Override
    public Map<String, List<String>> getWGPADocumentIDs() {
        return workingGroups.values().stream().collect(Collectors.toMap(WorkingGroup::alias, WorkingGroupsHelper::extractWGPADocumentIDs));
    }

    @Override
    public WorkingGroupCommittee getWGCommittee(String alias, String groupPrefix) {
        try {
            List<String> documentIds = getWGPADocumentIDs().get(alias);
            if (documentIds == null || documentIds.isEmpty()) {
                LOGGER.warn("No agreement documents found for WG '{}', committees cannot be mapped", TransformationHelper.formatLog(alias));
                return null;
            }

            List<SysRelationData> relations = getFilteredRelations(groupPrefix);
            // Size of 2 means only IG and CR type found, indicating invalid prefix
            if (relations == null || relations.isEmpty() || relations.size() == INVALID_PREFIX_COUNT) {
                LOGGER.warn("Could not find full committee relation mappings for prefix '{}'", TransformationHelper.formatLog(groupPrefix));
                return null;
            }

            List<FullOrganizationContactData> contacts = getOrgContacts(alias, documentIds);
            // Check if any contacts match current committee prefix
            boolean hasComitteeContact = contacts
                    .stream()
                    .anyMatch(c -> c.sysRelation().relation().toLowerCase().startsWith(groupPrefix.toLowerCase()));
            if (!hasComitteeContact) {
                LOGGER
                        .error("No committee/council members found for prefix code {} in working group with alias '{}'",
                                TransformationHelper.formatLog(groupPrefix), TransformationHelper.formatLog(alias));
                return null;
            }

            return new WorkingGroupCommittee(groupPrefix, alias, createCommitteeRole(CommitteePositionType.AP, relations, contacts),
                    createCommitteeRole(CommitteePositionType.AA, relations, contacts),
                    createCommitteeRole(CommitteePositionType.EP, relations, contacts),
                    createCommitteeRole(CommitteePositionType.EA, relations, contacts),
                    createCommitteeRole(CommitteePositionType.IG, relations, contacts),
                    createCommitteeRole(CommitteePositionType.CR, relations, contacts));

        } catch (Exception e) {
            LOGGER.error("ERROR WHILE PROCESSING WG-COMMITTEE:", e);
            return null;
        }
    }

    /**
     * Creates a Committee role object using the given committee position type, relations, and contact data. Fetches user EFUsername and org
     * logo data. Populates the members list with the respective People and Organization data.
     * 
     * @param type The current committee position type.
     * @param relations List of valid relations for the committee
     * @param contacts List of org contacts for the WG
     * @return A CommitteeRole entity.
     */
    private CommitteeRole createCommitteeRole(CommitteePositionType type, List<SysRelationData> relations,
            List<FullOrganizationContactData> contacts) {

        SysRelationData relation = relations
                .stream()
                .filter(r -> CommitteePositionType.getType(r.relation()) == type)
                .findFirst()
                .orElse(null);

        // Relations should all be valid at this point. Shouldn't return null
        if (relation == null) {
            throw new ServerErrorException(
                    String.format("Could not find relation code for type '%s' in list of relations for committee", type.getName()),
                    Status.INTERNAL_SERVER_ERROR);
        }

        List<CommitteeMember> members = contacts
                .stream()
                .filter(c -> c.sysRelation().equals(relation))
                .map(this::mapContactToCommitteeMember)
                .toList();

        return new CommitteeRole(relation.relation(), relation.description(), members);
    }

    /**
     * Converts a FullOrganizationContactData entity into a corresponding CommitteeMember record.
     * 
     * @param contact The FullOrganizationContactData entity to convert
     * @return The mapped CommiteeMember object.
     */
    private CommitteeMember mapContactToCommitteeMember(FullOrganizationContactData contact) {
        String name = contact.person().fname() + " " + contact.person().lname();
        String username = contact.person().personID();
        String orgName = contact.organization().name1();
        Integer orgId = contact.organization().organizationID();
        MemberOrganizationLogos logos = getOrgLogosById(contact.organization().organizationID());
        return new CommitteeMember(name, username, new MemberOrganization(orgName, orgId, logos, Collections.emptyList()));
    }

    /**
     * Gets the org logos from the cached api service based on the given orgid.
     * 
     * @param orgId The desired org's id.
     * @return A MemberOrganizationLogos entity or null.
     */
    private MemberOrganizationLogos getOrgLogosById(int orgId) {
        // Return null logos object if none found
        return getOrganizationById(orgId)
                .orElse(new MemberOrganization("", orgId, new MemberOrganizationLogos(null, null), Collections.emptyList()))
                .logos();
    }

    private List<SysRelationData> getFilteredRelations(String groupPrefix) {
        String relationType = config.relationType();

        // Paginated fetch of all relations matching the ME type
        Optional<List<SysRelationData>> relations = cache
                .get(relationType, new MultivaluedHashMap<>(), SysRelationData.class,
                        () -> middleware
                                .getAll(params -> fdbAPI.getRelations(params, config.relationType()))
                                .stream()
                                .filter(relation -> WorkingGroupsHelper.filterRelationsByPrefix(relation.relation(), groupPrefix))
                                .toList())
                .data();

        if (relations.isEmpty()) {
            LOGGER.warn("Could not find any Relations of type '{}', there may be a connection error", relationType);
            return Collections.emptyList();
        }

        return relations.get();
    }

    /**
     * Fetches all Full Organization Contacts from fdndb-api, matching against the org alias and participation agreement document ids.
     * Caches all results.
     * 
     * @param alias The current org alias
     * @param documentIds The WG participation agreements.
     * @return A list of FullOrganizationContactData entities if they exist.
     */
    private List<FullOrganizationContactData> getOrgContacts(String alias, List<String> documentIds) {

        // Paginated fetch of all org contacts that have signed any from a list of docs
        Optional<List<FullOrganizationContactData>> orgContacts = cache
                .get(alias, new MultivaluedHashMap<>(), FullOrganizationContactData.class,
                        () -> middleware.getAll(r -> fdbAPI.getFullContacts(r, documentIds)))
                .data();

        if (orgContacts.isEmpty()) {
            LOGGER.warn("Could not find any OrgContacts for '{}', there may be a connection error", TransformationHelper.formatLog(alias));
            return Collections.emptyList();
        }

        return orgContacts.get();
    }

    private Optional<MemberOrganization> getOrganizationById(int organizationId) {
        List<MemberOrganization> memberOrgs = cacheManager
                .getList(ParameterizedCacheKeyBuilder
                        .builder()
                        .id("all")
                        .clazz(MemberOrganization.class)
                        .params(new MultivaluedHashMap<>())
                        .build());
        return memberOrgs.stream().filter(m -> m.organizationId() == organizationId).findFirst();
    }
}
