/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Contains all custom configs used by this application. Includes the location of the working groups static file, as well as the default
 * committee member relation type.
 */
@ConfigMapping(prefix = "eclipse.working-groups")
public interface WorkingGroupsConfig {

    @WithDefault(value = "working_groups.json")
    String filepath();

    @WithDefault(value = "ME")
    String relationType();
}
