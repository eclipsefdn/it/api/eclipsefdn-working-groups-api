/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.namespace;

import java.util.stream.Stream;

/**
 * WG committee/council position types. These are committee-agnostic. Appointed primary, Appointed alternate, Elected primary, Elected
 * alternate, Invited guest, and Committer representative.
 */
public enum CommitteePositionType {
    AP, AA, EP, EA, IG, CR, UNSUPPORTED;

    public String getName() {
        return this.name().toLowerCase();
    }

    /**
     * Matches the given relation value with any of the supported types by matching the end of the string, ignoring committee prefix.
     * 
     * @param relation The given relation to match
     * @return A valid CommitteePositionType, or UNSUPPPORTED
     */
    public static CommitteePositionType getType(String relation) {
        if (relation == null) {
            return UNSUPPORTED;
        }

        return Stream.of(values()).filter(c -> relation.toLowerCase().endsWith(c.getName())).findFirst().orElse(UNSUPPORTED);
    }
}
