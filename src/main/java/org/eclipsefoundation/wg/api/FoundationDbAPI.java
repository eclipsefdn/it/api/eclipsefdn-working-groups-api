/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.vertx.http.Compressed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

/**
 * Api client bound to the foundationdb-api service. Fetches system relations and org contacts.
 */
@OidcClientFilter
@RegisterRestClient(configKey = "fdndb-api")
public interface FoundationDbAPI {

    /**
     * Fetches all system relations filtered by the given type.
     * 
     * @param baseParams Base request paramters used for pagination.
     * @param type The given relation type filter.
     * @return A Response containing the filtered relations
     */
    @GET
    @Compressed
    @Path("/sys/relations")
    RestResponse<List<SysRelationData>> getRelations(@BeanParam BaseAPIParameters baseParams, @QueryParam("type") String type);

    /**
     * Fetches all Full org contacts containing Person, Org, and SysRelation entities. Filtered by document ids.
     * 
     * @param baseParams Base request paramters used for pagination.
     * @param documentIds The given document id filters.
     * @return A Response containing the filtered org contacts.
     */
    @GET
    @Compressed
    @Path("/organizations/contacts/full")
    RestResponse<List<FullOrganizationContactData>> getFullContacts(@BeanParam BaseAPIParameters baseParams,
            @QueryParam("documentIDs") List<String> documentIds);
}
