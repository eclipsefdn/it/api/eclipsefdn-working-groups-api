/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.wg.models.MemberOrganization;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.vertx.http.Compressed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

/**
 * Api client bound to the membership-api. Used to fetch member organization data.
 */
@RegisterRestClient(configKey = "membership-api")
@ApplicationScoped
public interface MembershipAPI {

    /**
     * Returns a paginated list of all slimmed-down member orgs from the membership api service.
     * 
     * @param baseParams Base request paramters used for pagination.
     * @return A Response containing a list of member org objects.
     */
    @GET
    @Compressed
    @Path("organizations")
    RestResponse<List<MemberOrganization>> getWGMemberOrgs(@BeanParam BaseAPIParameters baseParams);
}
