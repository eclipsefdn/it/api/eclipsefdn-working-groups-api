/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.models;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Entity representing a working group member organization.
 */
public record MemberOrganization(String name, @JsonProperty(value = "organization_id") Integer organizationId, MemberOrganizationLogos logos,
        @JsonProperty(access = Access.WRITE_ONLY) List<WorkingGroupParticipationAgreement> wgpas) {

    public MemberOrganization {
        Objects.requireNonNull(name);
        Objects.requireNonNull(organizationId);
    }

    /**
     * Entity representing a member organization's logos.
     */
    public record MemberOrganizationLogos(String print, String web) {

    }

    /**
     * Entity representing a member organization's WGPAs on file.
     */
    public record WorkingGroupParticipationAgreement(String documentId, String description, String level, String workingGroup) {

    }
}
