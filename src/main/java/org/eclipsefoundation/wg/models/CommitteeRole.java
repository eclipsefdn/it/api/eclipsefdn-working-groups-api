/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.models;

import java.util.List;
import java.util.Objects;

/**
 * Entity representing a working group committee role.
 */
public record CommitteeRole(String code, String description, List<CommitteeMember> members) {

    public CommitteeRole {
        Objects.requireNonNull(code);
        Objects.requireNonNull(description);
        Objects.requireNonNull(members);
    }

    /**
     * Entity representing a working group committee member.
     */
    public record CommitteeMember(String name, String username, MemberOrganization organization) {

        public CommitteeMember {
            Objects.requireNonNull(name);
            Objects.requireNonNull(username);
            Objects.requireNonNull(organization);
        }
    }
}
