/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.models;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entity representing a working group.
 */
public record WorkingGroup(String alias, String title, String status, String logo, String description, String parentOrganization,
        WorkingGroupResources resources, List<WorkingGroupParticipationLevel> levels) {

    public WorkingGroup {
        Objects.requireNonNull(alias);
        Objects.requireNonNull(title);
        Objects.requireNonNull(status);
        Objects.requireNonNull(logo);
        Objects.requireNonNull(description);
        Objects.requireNonNull(parentOrganization);
        Objects.requireNonNull(resources);
        Objects.requireNonNull(levels);
    }

    /**
     * Entity representing a Working Group committee
     */
    public record WorkingGroupCommittee(String groupPrefix, String workingGroup, CommitteeRole appointedPrimary,
            CommitteeRole appointedAlternate, CommitteeRole electedPrimary, CommitteeRole electedAlternate, CommitteeRole invitedGuest,
            CommitteeRole committerRepresentative) {

        public WorkingGroupCommittee {
            Objects.requireNonNull(groupPrefix);
            Objects.requireNonNull(workingGroup);
        }
    }

    /**
     * Entity representing a working group participation agreement.
     */
    public record WorkingGroupParticipationAgreement(@JsonProperty("document_id") String documentId, String pdf) {

        public WorkingGroupParticipationAgreement {
            Objects.requireNonNull(documentId);
            Objects.requireNonNull(pdf);
        }
    }

    /**
     * Entity representing a working group's participation agreements.
     */
    public record WorkingGroupParticipationAgreements(WorkingGroupParticipationAgreement individual,
            WorkingGroupParticipationAgreement organization) {

    }

    /**
     * Entity representing a working group's resources.
     */
    public record WorkingGroupResources(String charter, String website, String members, String sponsorship, String contactForm,
            WorkingGroupParticipationAgreements participationAgreements) {

        public WorkingGroupResources {
            Objects.requireNonNull(charter);
            Objects.requireNonNull(website);
            Objects.requireNonNull(members);
            Objects.requireNonNull(sponsorship);
            Objects.requireNonNull(contactForm);
            Objects.requireNonNull(participationAgreements);
        }
    }

    /**
     * Entity representing a working group participation level.
     */
    public record WorkingGroupParticipationLevel(String relation, String description) {

        public WorkingGroupParticipationLevel {
            Objects.requireNonNull(relation);
            Objects.requireNonNull(description);
        }
    }
}
