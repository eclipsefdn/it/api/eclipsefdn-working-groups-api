/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.precache;

import java.util.Collections;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.wg.api.MembershipAPI;
import org.eclipsefoundation.wg.models.MemberOrganization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

@Named("member-orgs")
@ApplicationScoped
public class MemberOrganizationPrecacheProvider implements LoadingCacheProvider<MemberOrganization> {
    public static final Logger LOGGER = LoggerFactory.getLogger(MemberOrganizationPrecacheProvider.class);

    private final MembershipAPI membershipApi;
    private final APIMiddleware middleware;

    public MemberOrganizationPrecacheProvider(@RestClient MembershipAPI membershipApi, APIMiddleware middleware) {
        this.membershipApi = membershipApi;
        this.middleware = middleware;
    }

    @Override
    public List<MemberOrganization> fetchData(ParameterizedCacheKey k) {
        LOGGER.info("Loading all Member Orgs from membership");
        List<MemberOrganization> results;
        try {
            results = middleware.getAll(membershipApi::getWGMemberOrgs);
        } catch (Exception e) {
            LOGGER.error("Error while building member organizations list: ", e);
            return Collections.emptyList();
        }
        LOGGER.info("Loaded {} member organizations", results.size());
        return results;
    }

    @Override
    public Class<MemberOrganization> getType() {
        return MemberOrganization.class;
    }
}
