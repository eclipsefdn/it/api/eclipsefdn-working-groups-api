/** **************************************************************
 Copyright (C) 2024 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

 SPDX-License-Identifier: EPL-2.0
******************************************************************/

reportWorkingGroupLevels();
async function reportWorkingGroupLevels() {
  const wgs = await getWorkingGroups();
  console.log(wgsAsString(wgs));
}

const wgsAsString = (wgs: WorkingGroup[]) => {
  return wgs.map(wg => `${wg.title} levels:\n- ${wgLevelsAsString(wg.levels)}`).join('\n\n').toString();
}

const wgLevelsAsString = (levels: WorkingGroupLevel[]) => {
  return levels.map(level => `${level.description} (${level.relation})`).join('\n- ');
}

/**
 *
 * WORKING GROUP FETCH
 *
 */

interface WorkingGroup {
  alias: string;
  title: string;
  status: string;
  parent_organization: string;
  levels: WorkingGroupLevel[];
  resources: WorkingGroupResources;
}

interface WorkingGroupLevel {
  relation: string;
  description: string;
}

interface WorkingGroupResources {
  charter: string;
}

async function getWorkingGroups(host = 'https://api.eclipse.org'): Promise<WorkingGroup[]> {
  // retrieve unsecured resources. Does not have dev stubs as they aren't needed here
  return await typedFetch(host + '/working-groups');
}

/**
 * Retrieves basic resources and types them to the return. Will also allow for additional request properties to be set if needed.
 *
 * @param path the path to use to retrieve resources
 * @param properties optional map containing additional properties for the fetch, defaults to an empty object.
 * @returns a promise containing the data to be transformed.
 * @throws if the call is not successful, an error will be thrown
 */
function typedFetch<T>(path: string, properties: Record<string, object> = {}): Promise<T> {
  return fetch(path, properties).then(r => {
    if (!r.ok) {
      throw new Error(r.statusText);
    }
    return r.json() as Promise<T>;
  });
}