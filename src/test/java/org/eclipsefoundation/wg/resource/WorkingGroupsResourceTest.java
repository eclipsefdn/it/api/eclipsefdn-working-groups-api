/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.resource;

import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.eclipsefoundation.wg.test.helpers.SchemaNamespaceHelper;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class WorkingGroupsResourceTest {

    public static final String WGS_BASE_URL = "";

    public static final String WG_STATUS_URL = WGS_BASE_URL + "?status={param}";
    public static final String WG_STATUSES_URL = WGS_BASE_URL + "?status={param1}&status={param2}";
    
    public static final String WG_BY_ORGANIZATION_URL = WGS_BASE_URL + "/organization/{id}";

    public static final String WG_BASE_URL = WGS_BASE_URL + "/{alias}";

    public static final String WG_RESOURCES_URL = WG_BASE_URL + "/resources";
    public static final String WG_LEVELS_URL = WG_BASE_URL + "/levels";
    public static final String WG_AGREEMENTS_URL = WG_BASE_URL + "/agreements";
    public static final String WG_COMMITTEES_URL = WG_BASE_URL + "/committees/{relationType}";

    /*
     * GET_ALL
     */
    public static final EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper
            .buildSuccessCase(WGS_BASE_URL, new String[] {}, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    public static final EndpointTestCase GET_ALL_SINGLE_STATUS_SUCCESS = TestCaseHelper
            .buildSuccessCase(WG_STATUS_URL, new String[] { "active" }, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    public static final EndpointTestCase GET_ALL_MULTI_STATUS_SUCCESS = TestCaseHelper
            .buildSuccessCase(WG_STATUSES_URL, new String[] { "active", "archived" }, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    /*
     * GET_BY_ALIAS
     */
    public static final EndpointTestCase GET_BY_ALIAS_SUCCESS = TestCaseHelper
            .buildSuccessCase(WG_BASE_URL, new String[] { "awful-group" }, SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH);

    public static final EndpointTestCase GET_BY_ALIAS_404 = TestCaseHelper
            .buildNotFoundCase(WG_BASE_URL, new String[] { "invalid-Group" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);
    
    /*
     * GET_BY_ORGANIZATION
     */
    public static final EndpointTestCase GET_BY_ORGANIZATION_SUCCESS = TestCaseHelper
            .buildSuccessCase(WG_BY_ORGANIZATION_URL, new String[] { "11" }, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    public static final EndpointTestCase GET_BY_ORGANIZATION_404 = TestCaseHelper
            .buildNotFoundCase(WG_BY_ORGANIZATION_URL, new String[] { "12345" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);
    public static final EndpointTestCase GET_BY_ORGANIZATION_BAD_REQUEST = TestCaseHelper
            .buildBadRequestCase(WG_BY_ORGANIZATION_URL, new String[] { "ibm" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET_RESOURCES
     */
    public static final EndpointTestCase GET_RESOURCES_SUCCESS = TestCaseHelper
            .buildSuccessCase(WG_RESOURCES_URL, new String[] { "important-group" },
                    SchemaNamespaceHelper.WORKING_GROUP_RESOURCES_SCHEMA_PATH);

    public static final EndpointTestCase GET_RESOURCES_404 = TestCaseHelper
            .buildNotFoundCase(WG_RESOURCES_URL, new String[] { "invalid-Group" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET_LEVELS
     */
    public static final EndpointTestCase GET_LEVELS_SUCCESS = TestCaseHelper
            .buildSuccessCase(WG_LEVELS_URL, new String[] { "mediocre-group" }, SchemaNamespaceHelper.WORKING_GROUP_LEVELS_SCHEMA_PATH);

    public static final EndpointTestCase GET_LEVELS_404 = TestCaseHelper
            .buildNotFoundCase(WG_LEVELS_URL, new String[] { "invalid-Group" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET_AGREEMENTS
     */
    public static final EndpointTestCase GET_AGREEMENTS_SUCCESS = TestCaseHelper
            .buildSuccessCase(WG_AGREEMENTS_URL, new String[] { "important-group" },
                    SchemaNamespaceHelper.WORKING_GROUP_AGREEMENTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_AGREEMENTS_404 = TestCaseHelper
            .buildNotFoundCase(WG_AGREEMENTS_URL, new String[] { "invalid-Group" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET_COMMITTEE
     */
    public static final EndpointTestCase GET_COMMITTEES_SUCCESS_STEERING = TestCaseHelper
            .buildSuccessCase(WG_COMMITTEES_URL, new String[] { "important-group", "wmc" },
                    SchemaNamespaceHelper.WORKING_GROUP_COMMITTEE_SCHEMA_PATH);

    public static final EndpointTestCase GET_COMMITTEES_SUCCESS_MARKETING = TestCaseHelper
            .buildSuccessCase(WG_COMMITTEES_URL, new String[] { "mediocre-group", "wsc" },
                    SchemaNamespaceHelper.WORKING_GROUP_COMMITTEE_SCHEMA_PATH);

    public static final EndpointTestCase GET_COMMITTEE_INVALID_ALIAS = TestCaseHelper
            .buildBadRequestCase(WG_COMMITTEES_URL, new String[] { "invalid-Group", "WMC" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase GET_COMMITTEE_INVALID_GROUP = TestCaseHelper
            .buildBadRequestCase(WG_COMMITTEES_URL, new String[] { "mediocre-Group", "WSC" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET_ALL
     */
    @Test
    void getAll_success() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).run();
    }

    @Test
    void getAll_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).run();
    }

    @Test
    void getAll_success_matchingSpec() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).run();
    }

    @Test
    void getAllBySingleStatus_success() {
        EndpointTestBuilder.from(GET_ALL_SINGLE_STATUS_SUCCESS).run();
    }

    @Test
    void getAllBySingleStatus_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_SINGLE_STATUS_SUCCESS).run();
    }

    @Test
    void getAllBySingleStatus_success_matchingSpec() {
        EndpointTestBuilder.from(GET_ALL_SINGLE_STATUS_SUCCESS).run();
    }

    @Test
    void getAllByMultiStatus_success() {
        EndpointTestBuilder.from(GET_ALL_MULTI_STATUS_SUCCESS).run();
    }

    @Test
    void getAllByMultiStatus_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_MULTI_STATUS_SUCCESS).run();
    }

    @Test
    void getAllByMultiStatus_success_matchingSpec() {
        EndpointTestBuilder.from(GET_ALL_MULTI_STATUS_SUCCESS).run();
    }

    /*
     * GET_BY_ALIAS
     */
    @Test
    void getByAlias_success() {
        EndpointTestBuilder.from(GET_BY_ALIAS_SUCCESS).run();
    }

    @Test
    void getByAlias_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_BY_ALIAS_SUCCESS).run();
    }

    @Test
    void getByAlias_success_matchingSpec() {
        EndpointTestBuilder.from(GET_BY_ALIAS_SUCCESS).run();
    }

    @Test
    void getByAlias_failure_invalidName() {
        EndpointTestBuilder.from(GET_BY_ALIAS_404).run();
    }

    @Test
    void getByAlias_failure_invalidName_validateSchema() {
        EndpointTestBuilder.from(GET_BY_ALIAS_404).run();
    }
    
    /*
     * GET_BY_ORGANIZATION
     */
    @Test
    void getWorkingGroupsForOrganization_success() {
        EndpointTestBuilder.from(GET_BY_ORGANIZATION_SUCCESS).run();
    }

    @Test
    void getWorkingGroupsForOrganization_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_BY_ORGANIZATION_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getWorkingGroupsForOrganization_success_matchingSpec() {
        EndpointTestBuilder.from(GET_BY_ORGANIZATION_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getWorkingGroupsForOrganization_failure_noMatchingOrg() {
        EndpointTestBuilder.from(GET_BY_ORGANIZATION_404).run();
    }

    @Test
    void getWorkingGroupsForOrganization_failure_noMatchingOrg_validateSchema() {
        EndpointTestBuilder.from(GET_BY_ORGANIZATION_404).andCheckSchema().run();
    }
    @Test
    void getWorkingGroupsForOrganization_failure_invalidName() {
        EndpointTestBuilder.from(GET_BY_ORGANIZATION_BAD_REQUEST).run();
    }

    @Test
    void getWorkingGroupsForOrganization_failure_invalidName_validateSchema() {
        EndpointTestBuilder.from(GET_BY_ORGANIZATION_BAD_REQUEST).andCheckSchema().run();
    }

    /*
     * GET_RESOURCES
     */
    @Test
    void getResources_success() {
        EndpointTestBuilder.from(GET_RESOURCES_SUCCESS).run();
    }

    @Test
    void getResources_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_RESOURCES_SUCCESS).run();
    }

    @Test
    void getResources_success_matchingSpec() {
        EndpointTestBuilder.from(GET_RESOURCES_SUCCESS).run();
    }

    @Test
    void getResources_failure_invalidName() {
        EndpointTestBuilder.from(GET_RESOURCES_404).run();
    }

    @Test
    void getResources_failure_invalidName_validatSchema() {
        EndpointTestBuilder.from(GET_RESOURCES_404).run();
    }

    /*
     * GET_LEVELS
     */
    @Test
    void getLevels_success() {
        EndpointTestBuilder.from(GET_LEVELS_SUCCESS).run();
    }

    @Test
    void getLevels_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_LEVELS_SUCCESS).run();
    }

    @Test
    void getLevels_success_matchingSpec() {
        EndpointTestBuilder.from(GET_LEVELS_SUCCESS).run();
    }

    @Test
    void getLevels_failure_invalidName() {
        EndpointTestBuilder.from(GET_LEVELS_404).run();
    }

    @Test
    void getLevels_failure_invalidName_validateSchema() {
        EndpointTestBuilder.from(GET_LEVELS_404).run();
    }

    /*
     * GET_AGREEMENTS
     */
    @Test
    void getAgreements_success() {
        EndpointTestBuilder.from(GET_AGREEMENTS_SUCCESS).run();
    }

    @Test
    void getAgreements_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_AGREEMENTS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getAgreements_success_matchingSpec() {
        EndpointTestBuilder.from(GET_AGREEMENTS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getAgreements_failure_invalidName() {
        EndpointTestBuilder.from(GET_AGREEMENTS_404).run();
    }

    @Test
    void getAgreements_failure_invalidName_validateSchema() {
        EndpointTestBuilder.from(GET_AGREEMENTS_404).andCheckSchema().run();
    }

    /*
     * GET_COMMITTEE
     */
    @Test
    void getCommittees_success() {
        EndpointTestBuilder.from(GET_COMMITTEES_SUCCESS_STEERING).run();
        EndpointTestBuilder.from(GET_COMMITTEES_SUCCESS_MARKETING).run();
    }

    @Test
    void getCommittees_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_COMMITTEES_SUCCESS_STEERING).andCheckFormat().run();
        EndpointTestBuilder.from(GET_COMMITTEES_SUCCESS_MARKETING).andCheckFormat().run();
    }

    @Test
    void getCommittees_success_matchingSpec() {
        EndpointTestBuilder.from(GET_COMMITTEES_SUCCESS_STEERING).andCheckSchema().run();
        EndpointTestBuilder.from(GET_COMMITTEES_SUCCESS_MARKETING).andCheckSchema().run();
    }

    @Test
    void getCommittees_failure_invalidName() {
        EndpointTestBuilder.from(GET_COMMITTEE_INVALID_ALIAS).run();
    }

    @Test
    void getCommittees_failure_invalidName_validateSchema() {
        EndpointTestBuilder.from(GET_COMMITTEE_INVALID_ALIAS).andCheckSchema().run();
    }

    @Test
    void getCommittees_failure_invalidCommitteeType() {
        EndpointTestBuilder.from(GET_COMMITTEE_INVALID_GROUP).run();
    }

    @Test
    void getCommittees_failure_invalidCommitteeType_validateSchema() {
        EndpointTestBuilder.from(GET_COMMITTEE_INVALID_GROUP).andCheckSchema().run();
    }
}
