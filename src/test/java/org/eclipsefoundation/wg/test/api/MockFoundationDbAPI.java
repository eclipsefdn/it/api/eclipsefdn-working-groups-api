/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.eclipsefoundation.wg.api.FoundationDbAPI;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockFoundationDbAPI implements FoundationDbAPI {

    private List<SysRelationData> relations;
    private List<FullOrganizationContactData> orgContacts;

    public MockFoundationDbAPI() {
        relations = new ArrayList<>();
        relations
                .addAll(Arrays
                        .asList(generateSysRelationEntity("WMCAA", "WG - Marketing Committee Appointed Alternate"),
                                generateSysRelationEntity("WMCAP", "WG - Marketing Committee Appointed Primary"),
                                generateSysRelationEntity("WMCEA", "WG - Marketing Committee Elected Alternate"),
                                generateSysRelationEntity("WMCEP", "WG - Marketing Committee Elected Primary"),
                                generateSysRelationEntity("WSCAA", "WG - Marketing Committee Appointed Alternate"),
                                generateSysRelationEntity("WSCAP", "WG - Marketing Committee Appointed Primary"),
                                generateSysRelationEntity("WSCEA", "WG - Marketing Committee Elected Alternate"),
                                generateSysRelationEntity("WSCEP", "WG - Marketing Committee Elected Primary"),
                                generateSysRelationEntity("WGCR", "WG - Committer Representative Elected"),
                                generateSysRelationEntity("WPIG", "WG - Invited Guest")));

        orgContacts = new ArrayList<>();
        orgContacts
                .addAll(Arrays
                        .asList(generateFulOrgcontactentity("test", "01", 11, "WMCAA", "importantDoc"),
                                generateFulOrgcontactentity("test", "02", 22, "WMCAP", "importantDoc"),
                                generateFulOrgcontactentity("test", "03", 33, "WMCEA", "importantDoc"),
                                generateFulOrgcontactentity("test", "04", 44, "WMCEP", "importantDoc"),
                                generateFulOrgcontactentity("test", "05", 55, "WMCAP", "importantDoc"),
                                generateFulOrgcontactentity("test", "06", 66, "WMCAA", "importantDoc"),
                                generateFulOrgcontactentity("test", "07", 77, "WGCR", "importantDocIndividual"),
                                generateFulOrgcontactentity("test", "08", 11, "WPIG", "importantDoc"),
                                generateFulOrgcontactentity("test", "09", 11, "WSCEP", "mediocreDoc"),
                                generateFulOrgcontactentity("test", "10", 22, "WSCEA", "mediocreDoc"),
                                generateFulOrgcontactentity("test", "11", 22, "WSCAP", "mediocreDoc"),
                                generateFulOrgcontactentity("test", "12", 33, "WSCAA", "mediocreDoc")));
    }

    @Override
    public RestResponse<List<SysRelationData>> getRelations(BaseAPIParameters baseParams, String type) {
        return MockDataPaginationHandler.paginateData(baseParams, relations.stream().filter(r -> r.type().equalsIgnoreCase(type)).toList());
    }

    @Override
    public RestResponse<List<FullOrganizationContactData>> getFullContacts(BaseAPIParameters baseParams, List<String> documentIds) {
        return MockDataPaginationHandler
                .paginateData(baseParams,
                        orgContacts
                                .stream()
                                .filter(contact -> documentIds.stream().anyMatch(id -> id.equalsIgnoreCase(contact.documentID())))
                                .toList());
    }

    private FullOrganizationContactData generateFulOrgcontactentity(String first, String last, int orgId, String relation, String docId) {
        return new FullOrganizationContactData(generateOrganizationEntity(orgId), generatePersonEntity(first, last),
                findExistingRelation(relation), docId, "comment", "employee");
    }

    private PeopleData generatePersonEntity(String first, String last) {
        return new PeopleData(first + last, first, last, "XX", true, first + last + "@email.com", null, null, null, null, null, false,
                false, null, null, null, null, null, null);
    }

    private OrganizationData generateOrganizationEntity(int orgId) {
        return new OrganizationData(orgId, "org-" + orgId, null, null, null, null, null, null, null, null);
    }

    private SysRelationData generateSysRelationEntity(String relation, String description) {
        return new SysRelationData(relation, description, true, "ME", null);
    }

    private SysRelationData findExistingRelation(String relation) {
        return relations.stream().filter(s -> s.relation().equalsIgnoreCase(relation)).findFirst().get();
    }
}
