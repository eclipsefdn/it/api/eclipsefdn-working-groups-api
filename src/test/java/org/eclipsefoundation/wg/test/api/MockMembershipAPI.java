/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.eclipsefoundation.wg.api.MembershipAPI;
import org.eclipsefoundation.wg.models.MemberOrganization;
import org.eclipsefoundation.wg.models.MemberOrganization.MemberOrganizationLogos;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockMembershipAPI implements MembershipAPI {

    private List<MemberOrganization> memberOrgs;

    public MockMembershipAPI() {
        this.memberOrgs = new ArrayList<>();
        this.memberOrgs
                .addAll(Arrays
                        .asList(new MemberOrganization("org", 11, new MemberOrganizationLogos("url", "url"), Collections.emptyList()),
                                new MemberOrganization("fake company", 22, new MemberOrganizationLogos("url", "url"),
                                        Collections.emptyList()),
                                new MemberOrganization("real company", 33, new MemberOrganizationLogos("url", "url"),
                                        Collections.emptyList()),
                                new MemberOrganization("other org", 44, new MemberOrganizationLogos("url", "url"), Collections.emptyList()),
                                new MemberOrganization("fake inc", 55, new MemberOrganizationLogos("url", "url"), Collections.emptyList()),
                                new MemberOrganization("org name", 66, new MemberOrganizationLogos("url", "url"), Collections.emptyList()),
                                new MemberOrganization("org inc", 77, new MemberOrganizationLogos("url", "url"), Collections.emptyList()),
                                new MemberOrganization("company foundation", 88, new MemberOrganizationLogos("url", "url"),
                                        Collections.emptyList())));
    }

    @Override
    public RestResponse<List<MemberOrganization>> getWGMemberOrgs(BaseAPIParameters baseParams) {
        return MockDataPaginationHandler.paginateData(baseParams, memberOrgs);
    }
}
