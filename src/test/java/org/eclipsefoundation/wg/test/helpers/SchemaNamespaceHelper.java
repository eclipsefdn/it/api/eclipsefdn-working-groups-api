/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.wg.test.helpers;

public class SchemaNamespaceHelper {

    public static final String BASE_SCHEMA_PATH = "schemas/";
    public static final String BASE_SCHEMA_PATH_SUFFIX = "-schema.json";

    public static final String WORKING_GROUP_SCHEMA_PATH = BASE_SCHEMA_PATH + "working-group" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String WORKING_GROUPS_SCHEMA_PATH = BASE_SCHEMA_PATH + "working-groups" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String WORKING_GROUP_RESOURCES_SCHEMA_PATH = BASE_SCHEMA_PATH + "working-group-resources" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String WORKING_GROUP_LEVELS_SCHEMA_PATH = BASE_SCHEMA_PATH + "working-group-levels" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String WORKING_GROUP_AGREEMENTS_SCHEMA_PATH = BASE_SCHEMA_PATH + "working-group-participation-agreements"
            + BASE_SCHEMA_PATH_SUFFIX;

    public static final String WORKING_GROUP_AGREEMENT_SCHEMA_PATH = BASE_SCHEMA_PATH + "working-group-participation-agreement"
            + BASE_SCHEMA_PATH_SUFFIX;

    public static final String WORKING_GROUP_COMMITTEE_SCHEMA_PATH = BASE_SCHEMA_PATH + "working-group-committee" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String ERROR_SCHEMA_PATH = BASE_SCHEMA_PATH + "error" + BASE_SCHEMA_PATH_SUFFIX;
}