
/** **************************************************************
 Copyright (C) 2023 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/
const argv = require('yargs')
  .usage('Usage: $0 [options]')
  .example('$0', '')
  .option('f', {
    alias: 'file',
    description: 'The location of the JSON file to test. Should be readable by the current user',
  }).argv;
const { z } = require('zod');
const fs = require('fs');


const WGPA_FORMAT = z.object({
  document_id: z.string(),
  pdf: z.string().url()
}).nullable();
const WORKING_GROUP_SCHEMA = z.object({
  working_groups: z.record(z.string(), z.object({
    alias: z.string(),
    title: z.string(),
    status: z.string(),
    parent_organization: z.string(),
    logo: z.string().url(),
    description: z.string(),
    resources: z.object({
      charter: z.string().url().or(z.string().max(0)),
      participation_agreements: z.object({
        individual: WGPA_FORMAT,
        organization: WGPA_FORMAT
      }),
      website: z.string().url().or(z.string().max(0)),
      members: z.string().url().or(z.string().max(0)),
      sponsorship: z.string().url().or(z.string().max(0)),
      contact_form: z.string().url().or(z.string().max(0))
    }),
    levels: z.array(z.object({
      relation: z.string().max(5),
      description: z.string()
    }))
  })),
});

// attempt to parse the passed file as override JSON
try {
  const out = castJsonToFormat(fs.readFileSync(argv.f, { encoding: 'UTF-8' }));
  // let the user know what we found generally.
  console.log(`Successfully parsed ${Object.keys(out.working_groups).length} working groups`);
} catch (e) {
  console.log(`Error encountered while processing JSON at ${argv.f}: ` + e);
  // error out if bad
  process.exit(1);
}

function castJsonToFormat(jsonString) {
  return WORKING_GROUP_SCHEMA.parse(JSON.parse(jsonString));
}
